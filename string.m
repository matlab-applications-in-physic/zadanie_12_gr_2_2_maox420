cd 'C:\Users\student\Desktop';
clear
G=79.3e9; %Rigidity modulus in [Pa] https://www.engineeringtoolbox.com/modulus-rigidity-d_946.html
r=2e-3;
R=2*2.54;
m=1e-2;
tau=2;

alpha0=9.81/m;
beta=1/2*tau;

N=2:50;
omega=0:10:480;

fileID = fopen('resonance_analysis_results.dat','w');
fprintf(fileID,'G=%.0f; r=%.3f; R=%1.2f; m=%0.2f; tau=%1.0f\n',G,r,R,m,tau);

for i=1:size(N,2)
    k=(G*r^4)/4*i*R^3;
    omega_0=sqrt(k/m);
    for t=1:size(omega,2)
        y(t) = alpha0/ sqrt((omega_0^2 - omega(t)^2)^2 + 4 * beta^2 * omega(t)^2);
    end
    
    PeakAmplitudes(i) = max(y(:,i));
    
    fprintf(fileID,'N=%d; A(N)=%0.3f; f=%0.3f\n',N(i),y(i),PeakAmplitudes(i));

end

fclose(fileID);

subplot(2,1,1);
plot(omega/2/pi,y)
xlabel('Oscillation frequency [Hz]');
ylabel('Amplitude [m]');
    
subplot(2,1,2);
plot(N,PeakAmplitudes)
xlabel('Number of turns (N)');
ylabel('Maximum amplitude [m]');

saveas(gcf, 'resonance.pdf');
